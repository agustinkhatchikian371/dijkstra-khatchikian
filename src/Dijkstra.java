import java.util.Arrays;
public class Dijkstra {
    private int V; // Número de vértices
    private int[][] matrizAdyacencia;

    public Dijkstra(int V) {
        this.V = V;
        matrizAdyacencia = new int[V][V];
        for (int i = 0; i < V; i++) {
            Arrays.fill(matrizAdyacencia[i], -1);
        }
    }

    public void agregarArista(int origen, int destino, int peso) {
        matrizAdyacencia[origen][destino] = peso;
    }

    public void dijkstra(int origen, int destino) {
        int[] distancias = new int[V];
        boolean[] visitados = new boolean[V];

        Arrays.fill(distancias, Integer.MAX_VALUE);
        distancias[origen] = 0;

        int[] camino = new int[V];
        camino[origen] = -1;

        for (int i = 0; i < V - 1; i++) {
            int nodoMinimo = encontrarNodoMinimo(distancias, visitados);
            visitados[nodoMinimo] = true;

            for (int j = 0; j < V; j++) {
                if (!visitados[j] && matrizAdyacencia[nodoMinimo][j] != -1 &&
                        distancias[nodoMinimo] != Integer.MAX_VALUE &&
                        distancias[nodoMinimo] + matrizAdyacencia[nodoMinimo][j] < distancias[j]) {
                    distancias[j] = distancias[nodoMinimo] + matrizAdyacencia[nodoMinimo][j];
                    camino[j] = nodoMinimo;
                }
            }
        }

        // Imprimir las distancias más cortas desde el nodo fuente a todos los demás nodos
        System.out.println("Distancias más cortas desde el nodo " + origen + ":");
        for (int i = 0; i < V; i++) {
            System.out.println("Nodo " + i + ": " + distancias[i]);
        }

        // Esto se hace para intentar imprimir el camino más corto entre origen y destino
        if (distancias[destino] == Integer.MAX_VALUE) {
            System.out.println("No hay un camino desde " + origen + " a " + destino);
        }
        else
        {
            System.out.println("Distancia mínima desde " + origen + " a " + destino + ": " + distancias[destino]);
            System.out.print("Camino: " + destino);
            int verticeActual = destino;
            // Aquí se recupera el camino ma corto desde el destino al origen
            while (camino[verticeActual] != -1) {
                System.out.print(" <- " + camino[verticeActual]);
                verticeActual = camino[verticeActual];
            }
        }
    }

    private int encontrarNodoMinimo(int[] distancias, boolean[] visitados) {
        int minDistancia = Integer.MAX_VALUE;
        int nodoMinimo = -1;

        for (int i = 0; i < V; i++) {
            if (!visitados[i] && distancias[i] < minDistancia) {
                minDistancia = distancias[i];
                nodoMinimo = i;
            }
        }

        return nodoMinimo;
    }
}